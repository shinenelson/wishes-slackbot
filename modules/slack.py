import json
import requests

class slack :

    conf = {}

    def slackAPI(self, endpoint, query):
        global conf
        try:
            r = requests.get(conf["url"] + endpoint + "?token=" + conf["token"] + "&" + query)
            return json.loads(r.text)
        except Exception as e:
            print(e)
            exit("Failed to execute the API request.")

    def load_users(self):
        users = []
        all_users = self.slackAPI("users.list","")
        if all_users["ok"] :
            for user in all_users["members"]:
                if all( filter in user and user[filter] == value for filter, value in conf["user_filters"].items() ):
                    users.append(user)
        return users

    def find_user(self, name, users):
        for user in users:
            if user["profile"]["real_name_normalized"].replace(" ", "").lower() == name.replace(" ","").lower():
                return user
        return None

    def send_message(self, user, message):
        print("User: {}\nMessage: {}\n\n".format(user,message))
        self.slackAPI("chat.postMessage","channel={}&text={}".format(user,message))

    def init(self, slack_conf):
        global conf
        conf =  slack_conf