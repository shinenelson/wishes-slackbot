# Wishes Slackbot

This bot wishes your team mates on Slack. It can send a message to a Slack channel of your choosing. You can even mention the user and send a custom greeting message.

The bot is designed to run as a `cron`job.

## Inspiration
This project was by [slack-birthday-bot](https://github.com/jeKnowledge/slack-birthday-bot) from which [@shinenelson](https://github.com/shinenelson) ported it into [a Python script](https://gist.github.com/shinenelson/2861380e2ecb5f4e14d8b42ac231cc74). The reasoning behind the port was to reduce the [barriers to entry](https://en.wikipedia.org/wiki/Barriers_to_entry) for deployment. A python runtime is generally available on *NIX-based systems out-of-the-box making it easier to deploy with minimal effort compared to Ruby which would require at least a ruby runtime to be installed on the system the script is being deployed on. From the python script, he got feedback to develop the project to work for anniversaries. After thinking about the data structure to fit for the new requirement, he got a bigger idea with a bigger scope which evolved into wishes slackbot.

## License
This script is released into the public domain. See the attached LICENSE file for more information.
